/*
 * <Basic 2D object model class with virtual drawing method (ancestral implementation)>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/ObjectModel2D.h"

/*
 * Function moveTo
 */
void lighthouse::ObjectModel2D::moveTo (const clan::Point& newPos)
{
    bool xset = false, yset = false;
    if(newPos.x < 0)
    {
        position.x = 0;
        xset = true;
    }
    if(newPos.y < 0)
    {
        position.y = 0;
        yset = true;
    }
    if(newPos.x + size.width > lighthouse::LighthouseGlobal::getLevelSize().width)
    {
        position.x = lighthouse::LighthouseGlobal::getLevelSize().width - size.width;
        xset = true;
    }
    if(newPos.y + size.height > lighthouse::LighthouseGlobal::getLevelSize().height)
    {
        position.y = lighthouse::LighthouseGlobal::getLevelSize().height - size.height;
        yset = true;
    }
    if(!xset)
    {
        position.x = newPos.x;
    }
    if(!yset)
    {
        position.y = newPos.y;
    }
    
}

/*
 * Constructor
 */
lighthouse::ObjectModel2D::ObjectModel2D (
    const std::string& pCommonSubdir,
    const unsigned int& pResourcesNum,
    const std::string* pSubpathArray,
    const clan::Point& initialAnchor,
    const clan::Size& pSize,
    bool initialSolidity,
    const OutlineAccuracy& pAccuracy
) :
    position(clan::Point(0, 0)),
    size(pSize),
    solid(initialSolidity),
    currentModelIndex(0),
    resCatalog(
        pResourcesNum,
        pCommonSubdir,
        pSubpathArray,
        pAccuracy)
{
    //Check position
    this->moveTo(initialAnchor);
    
}

void lighthouse::ObjectModel2D::setModelIndex(const unsigned int& newIdx)
{
    if(newIdx >= resCatalog.getResNum())
    {
        throw(std::out_of_range("lighthouse::ObjectModel2D::setModelIndex"));
    }
    currentModelIndex = newIdx;
}
