/*
 * <Sprite/collider collection object for handling image assets (implementation)>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/ResourceCatalog.h"

lighthouse::ResourceCatalog::ResourceCatalog(
    const unsigned int& pResourcesNum,
    const std::string& pCommonSubdir,
    const std::string* pSubpathArray,
    const OutlineAccuracy& pColliderAccuracy
) :
    resourcesNum(pResourcesNum),
    commonSubdir(pResourcesNum == 0 ? "" : pCommonSubdir),
    colliderAccuracy(pColliderAccuracy),
    array(nullptr)
{
    if(resourcesNum > 0)
    {
        //allocating memory for array
        array = new model[resourcesNum];
        /*if(!array) throw lighthouse::out_of_mem("Sprite loading");*//*exceptional exception handling*/
        
        //loading subpaths
        {
            std::string msg = "\tInspecting filesystem";
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        if(!pSubpathArray)
        {
            throw(std::invalid_argument("lighthouse::ResourceCatalog::ResourceCatalog(const std::string* pSubpathArray): array invalid"));
        }
        for (int i = 0; i < resourcesNum; ++i) {
            if(pSubpathArray[i] == "")
            {
                std::stringstream msgStream("lighthouse::ResourceCatalog::ResourceCatalog(const std::string* pSubpathArray): ");
                msgStream << i << "th value empty";
                throw(std::invalid_argument(msgStream.str()));
            }
            array[i].subpath = std::string(pSubpathArray[i]);
        }
        
        //path buffer
        std::string spritePathTemp;
        std::string collPathTemp;
        //Sprite processing loop
        for (int i = 0; i < resourcesNum; ++i)
        {
            //constructing Sprite path I. (filesystem)
            spritePathTemp = lighthouse::LighthouseGlobal::getSpritesFs().get_path();
            //constructing Sprite path II. (subdir)
            if(spritePathTemp[spritePathTemp.length()-1] != '/')
            {
                spritePathTemp += "/";
            }
            spritePathTemp += commonSubdir;
            //constructing Sprite path III. (subpath)
            spritePathTemp += array[i].subpath;
            //constructing Sprite path IV. (extension)
            spritePathTemp += ".png";
            
            //loading sprite
            {
                std::string msg = "\tLoading Sprite from " + spritePathTemp;
                lighthouse::LighthouseGlobal::writeLog(msg);
            }
            array[i].sprite = clan::Sprite(lighthouse::LighthouseGlobal::getCanvas(), spritePathTemp, lighthouse::LighthouseGlobal::getSpritesFs());
            array[i].sprite.set_alignment(clan::origin_bottom_left);
            array[i].sprite.set_linear_filter(false);
            array[i].sprite.set_rotation_hotspot(clan::origin_center);
            array[i].sprite.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
            
            //constructing Collider path I. (filesystem)
            collPathTemp = lighthouse::LighthouseGlobal::getCollidersFs().get_path();
            //constructing Collider path II. (subdir)
            if(collPathTemp[collPathTemp.length()-1] != '/')
            {
                collPathTemp += "/";
            }
            collPathTemp += commonSubdir;
            //constructing Collider path III. (subpath)
            collPathTemp += array[i].subpath;
            //constructing Collider path IV: (extension)
            collPathTemp += ".ccdo";
            //constructing Collider path V. (trimming filename)
            //looking for existing collider
            clan::FileSystem searchFs(collPathTemp.substr(0, collPathTemp.find_last_of("/")+1), false);
            if(searchFs.has_file(collPathTemp.substr(collPathTemp.find_last_of("/")+1)))
            {
                //loading existing collider
                {
                    std::string msg = "\tLoading Collider from " + collPathTemp;
                    lighthouse::LighthouseGlobal::writeLog(msg);
                }
                array[i].outline = CollisionOutline(collPathTemp.substr(collPathTemp.find_last_of("/")+1), searchFs);
            }
            else
            {
                //generating collider if not found
                {
                    std::string msg = "\tGenerating Collider for " + spritePathTemp;
                    lighthouse::LighthouseGlobal::writeLog(msg);
                }
                array[i].outline = CollisionOutline(spritePathTemp, lighthouse::LighthouseGlobal::getSpritesFs(), 128, pColliderAccuracy);
                array[i].outline.set_alignment(clan::origin_top_left);
                array[i].outline.set_rotation_hotspot(clan::origin_center);
                array[i].outline.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
                array[i].outline.save(collPathTemp.substr(collPathTemp.find_last_of("/")+1), searchFs);
            }
            
        }
        
    }
    
}

lighthouse::ResourceCatalog::~ResourceCatalog()
{
    delete[] array;
    array = nullptr;
}

lighthouse::model& lighthouse::ResourceCatalog::getModelByIndex(const unsigned int& index)
{
    if(index >= resourcesNum)
    {
        throw(std::out_of_range("lighthouse::ResourceCatalog::getModelByIndex()"));
    }
    return array[index];
}

void lighthouse::ResourceCatalog::rescale()
{
    if(resourcesNum > 0)
    {
        for (int i = 0; i < resourcesNum; ++i) {
            array[i].sprite.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
            array[i].outline.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
        }
    }
}

