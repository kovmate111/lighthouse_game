/*
 * <Level bas class implementation>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/Level.h"

lighthouse::Level::Level(
    const clan::Size& pLevelSize,
    clan::FileSystem& pLevelDataFs,
    const clan::Point& pPlayerInitialAnchor
) :
    levelSize(pLevelSize),
    levelDataFs(pLevelDataFs),
    initialPlayerAnchor(pPlayerInitialAnchor)
{
    //Exporting levelSize to the Global
    lighthouse::LighthouseGlobal::setLevelSize(levelSize);
    //Setting sprite and collider
    if(!(levelDataFs.has_file("background.png"))) 
    {
        throw(std::invalid_argument("lighthouse::Level::Level(pLevelDataFs)"));
    }
    {
        std::string msg = "Loading level background from " + levelDataFs.get_path() + "background.png";
        lighthouse::LighthouseGlobal::writeLog(msg);
    }
    levelBackground = clan::Sprite(lighthouse::LighthouseGlobal::getCanvas(), "background.png", levelDataFs);
    levelBackground.set_linear_filter(false);
    levelBackground.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
    if(levelDataFs.has_file("collider.cclo"))
    {
        {
            std::string msg = "Loading level terrain collider from " + levelDataFs.get_path() + "collider.cclo";
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        terrainCollider = CollisionOutline("collider.cclo", levelDataFs);
    }
    else
    {
        if(!(levelDataFs.has_file("foreground-mask.png")))
        {
            throw(std::invalid_argument("lighthouse::Level::Level(pLevelDataFs)"));
        }
        {
            std::string msg = "Generating level terrain collider from mask " + levelDataFs.get_path() + "foreground-mask.png";
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        terrainCollider = CollisionOutline("foreground-mask.png", levelDataFs, 128, OutlineAccuracy::accuracy_medium);
        terrainCollider.save("collider.cclo", levelDataFs);
    }
    
    //Calculating screen anchor
    //Assuming 240x135
    unsigned scrX, scrY;
    if(initialPlayerAnchor.x >= 104 && initialPlayerAnchor.x <= levelSize.width - 136)
    {
        scrX = initialPlayerAnchor.x - 104;
    }
    else if(initialPlayerAnchor.x < 104)
    {
        scrX = 0;
    }
    else
    {
        scrX = levelSize.width - 240;
    }
    if(initialPlayerAnchor.y >= 51 && initialPlayerAnchor.y <= levelSize.height - 84)
    {
        scrY = initialPlayerAnchor.y - 51;
    }
    else if(initialPlayerAnchor.y < 51)
    {
        scrY = 0;
    }
    else
    {
        scrY = levelSize.height - 135;
    }
    lighthouse::LighthouseGlobal::setScreenAnchor(clan::Point(scrX, scrY));
}
