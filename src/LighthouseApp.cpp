/*
 * <Main app class for the Lighthouse game.>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/LighthouseApp.h"

clan::ApplicationInstance<lighthouse::LighthouseApp> clanapp;

clan::DisplayWindowDescription lighthouse::LighthouseApp::makeLighthouseWindow()
{
    {
        std::string msg( "Setting up main window");
        lighthouse::LighthouseGlobal::writeLog(msg);
    }
    clan::DisplayWindowDescription mainWindowDesc;
    mainWindowDesc.set_allow_resize(false);
    mainWindowDesc.set_fullscreen(true, 0);
    mainWindowDesc.set_title("Lighthouse DEMO");
    mainWindowDesc.set_visible(true);
    return mainWindowDesc;
}

lighthouse::LighthouseApp::LighthouseApp() :
    mainWindow(lighthouse::LighthouseApp::makeLighthouseWindow()),
    mainCanvas(mainWindow),
    gameTime(24, 24),
    testLevel(),
    player(testLevel.getPlayerAnchor(), nullptr)
{
    // Exporting global values
    sprites = clan::FileSystem("../data/assets/");
    colliders = clan::FileSystem("../data/colliders");
    lighthouse::LighthouseGlobal::init(sprites, colliders, mainCanvas);
    try {
        {
            std::string msg( "Setting up openGL or DirectX");
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
#if defined(WIN32) && !defined(__MINGW32__)
        clan::D3DTarget::set_current();
#else
        clan::OpenGLTarget::set_current();
#endif
        
        {
            std::string msg( "Setting up upscaling");
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        lighthouse::LighthouseGlobal::setUpscaleFactor(static_cast<float>(mainCanvas.get_width() / 240.0));
        player.rescale();
        testLevel.rescale();
        
        {
            std::string msg( "Mapping buttons");
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        //Key mappings
        //TODO add ability to remap keys
        walkLeftKey = clan::keycode_a;
        walkRightKey = clan::keycode_d;
        jumpKey = clan::keycode_space;
        interactKey = clan::keycode_w;
        
        {
            std::string msg( "Setting up event sockets and window close action");
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        slotContainer.connect(mainWindow.sig_window_close(), [&](){quit = true;});
        //slotContainer.connect(mainWindow.get_keyboard().sig_key_down(), clan::bind_member(this, &LighthouseApp::on_input_down));
        slotContainer.connect(mainWindow.get_keyboard().sig_key_up(), clan::bind_member(this, &LighthouseApp::on_input_up));
        
        {
            std::string msg( "Starting game timer");
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        gameTime.reset();
    } catch (std::invalid_argument arg) {
        {
            std::string msg("Invalid argument at "); msg += arg.what();
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        quit = true;
    } catch (std::out_of_range our) {
        {
            std::string msg("Index out of range at "); msg += our.what();
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        quit = true;
    }
    
}

void lighthouse::LighthouseApp::on_input_down(const clan::InputEvent& key)
{
    if(key.id == walkLeftKey)
    {
        player.moveLeft();
    }
    else if(key.id == walkRightKey)
    {
        player.moveRight();
    }
    else if(key.id == jumpKey)
    {
        //player.jump(); isabling for now
    }
    else if(key.id == interactKey)
    {
        //interact logic 
        // (calculate which Interactives are in radius and pass it to the player's begin_interact()
        // function, which in turn passes a signal to the Interactive object.)
        //in the case of the DEMO it is box1
        player.startInteract(testLevel.getNearestInteractive(player));
    }
}

void lighthouse::LighthouseApp::on_input_up(const clan::InputEvent& key)
{
    if(key.id == walkLeftKey || key.id == walkRightKey)
    {
        player.hStop();
    }
    else if(key.id == jumpKey)
    {
        if(player.getVertical() == lighthouse::vMovementState::jump)
        player.freefall();
    }
    else if(key.id == interactKey)
    {
        // interact logic
        //same but with end_interact() function.
        player.stopInteract();
    }
    else if(key.id == clan::keycode_escape)
    {
        {
            std::string msg( "ESC pressed, quitting");
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        quit = true;
    }
}

bool lighthouse::LighthouseApp::update()
{
    try {
        // Update game timer
        gameTime.update();
        
        // drawing and position updating logic
        mainCanvas.clear(clan::Colorf::black);
        testLevel.update(player);
        
        // Send the frame to the screen
        mainWindow.flip(1);
    } catch (std::out_of_range our) {
        {
            std::string msg("Out of range at "); msg += our.what();
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        quit=true;
    } catch (std::logic_error cst) {
        {
            std::string msg("Internal state corrupted at "); msg += cst.what();
            lighthouse::LighthouseGlobal::writeLog(msg);
        }
        quit=true;
    }
    //Keep the program alive (or not if that is what the user wants)
    return !quit;
}
