/*
 * <Object intefaces implementations>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/ObjectInterfaces.h"

lighthouse::Interactive::Interactive(
    const std::string& pCommonSubdir,
    const unsigned int& pResourcesNum,
    const std::string* pSubpathArray,
    const clan::Point& initialAnchor,
    bool initialSolidity,
    const std::vector<bool> pFlags
) :
    ObjectModel2D(
        pCommonSubdir,
        pResourcesNum,
        pSubpathArray,
        initialAnchor,
        clan::Size(16, 16),
        initialSolidity,
        OutlineAccuracy::accuracy_poor
    ),
    carryable(pFlags[0]),
    lightsource(pFlags[1]),
    lightreactive(pFlags[2]),
    steamsource(pFlags[3]),
    steamreactive(pFlags[4])
{}

lighthouse::ICarryable::ICarryable(
    const std::string& pCommonSubdir,
    const unsigned int& pResourcesNum,
    const std::string* pSubpathArray,
    const clan::Point& initialAnchor,
    bool initialSolidity,
    const std::vector<bool> pFlags
) :
    Interactive(
        pCommonSubdir,
        pResourcesNum,
        pSubpathArray,
        initialAnchor,
        initialSolidity,
        std::vector<bool>({true, pFlags[0], pFlags[1], pFlags[2], pFlags[3]})
    ),
    inHand(false),
    falling(false)
{}

