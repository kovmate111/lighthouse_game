/*
 * <Player class implementation>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/Player.h"

lighthouse::Player::Player (
    const clan::Point& initialAnchor,
    lighthouse::Interactive* initialHandState
) :
    ObjectModel2D(
        std::string("character/"),
        36,
        subpaths,
        initialAnchor,
        clan::Size(32, 32),
        true,
        OutlineAccuracy::accuracy_high
    ),
    hand(initialHandState),
    hSpeed(hMovementState::standRight),
    vSpeed(vMovementState::stand),
    animationCounter(0) //0 when standing
{
    //Not much to do here
}

void lighthouse::Player::draw()
{
    //Anchor is calced by update()
    //I only have to translate it and draw here.
    getSprite().draw(lighthouse::LighthouseGlobal::getCanvas(), (getAnchor().x - lighthouse::LighthouseGlobal::getScreenAnchor().x)*lighthouse::LighthouseGlobal::getUpscaleFactor(), (getAnchor().y - lighthouse::LighthouseGlobal::getScreenAnchor().y)*lighthouse::LighthouseGlobal::getUpscaleFactor());
}

void lighthouse::Player::update()
{
    // See whether moving left
    //No jumping just yet as there is no animation for it
    if(hSpeed == hMovementState::left && vSpeed == vMovementState::stand)
    { //Walking left
        if(getAnchor().x <= 0)
        {
            hStop();
        }
        else
        {
            /* Walking left: frame 0 is standing
             * frame  |  picture  |  move
             * -------|-----------|--------
             *     1  |   walk-1  |  O
             *     2  |   walk-1  |  
             *     3  |   walk-1  |  O
             *     4  |   walk-2  |  O
             *     5 ...
             */
            
            switch (animationCounter)
            {
                case 1:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(2);
                    break;
                case 2:
                    setModelIndex(2);
                    break;
                case 3:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(2);
                    break;
                case 4:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(3);
                    break;
                case 5:
                    setModelIndex(3);
                    break;
                case 6:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(3);
                    break;
                case 7:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(4);
                    break;
                case 8:
                    setModelIndex(4);
                    break;
                case 9:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(4);
                    break;
                case 10:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(5);
                    break;
                case 11:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(5);
                    break;
                case 12:
                    setModelIndex(5);
                    break;
                case 13:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(6);
                    break;
                case 14:
                    setModelIndex(6);
                    break;
                case 15:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(6);
                    break;
                case 16:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(7);
                    break;
                case 17:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(7);
                    break;
                case 18:
                    setModelIndex(7);
                    break;
                case 19:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(8);
                    break;
                case 20:
                    setModelIndex(8);
                    break;
                case 21:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(8);
                    break;
                case 22:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(9);
                    break;
                case 23:
                    setModelIndex(9);
                    break;
                case 24:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(9);
                    break;
                default:
                    animationCounter = 0;
                    break;
            }
            animationCounter++;
            if(carrying)
            {
                setModelIndex(getModelIndex() + 18);
            }
            
        }
        
        
    }
    if(hSpeed == hMovementState::right && vSpeed == vMovementState::stand)
    { //walking right
        if(getAnchor().x >= lighthouse::LighthouseGlobal::getLevelSize().width)
        {
            hStop();
        }
        else
        {
            /* Walking left: frame 0 is standing
             * frame  |  picture  |  move
             * -------|-----------|--------
             *     1  |   walk-1  |  O
             *     2  |   walk-1  |  
             *     3  |   walk-1  |  O
             *     4  |   walk-2  |  O
             *     5 ...
             */
            
            animationCounter++;
            switch (animationCounter)
            {
                case 1:
                    moveTo(clan::Point(getAnchor().x + 1, getAnchor().y));
                    setModelIndex(10);
                    break;
                case 2:
                    setModelIndex(10);
                    break;
                case 3:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(10);
                    break;
                case 4:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(11);
                    break;
                case 5:
                    setModelIndex(11);
                    break;
                case 6:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(11);
                    break;
                case 7:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(12);
                    break;
                case 8:
                    setModelIndex(12);
                    break;
                case 9:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(12);
                    break;
                case 10:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(13);
                    break;
                case 11:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(13);
                    break;
                case 12:
                    setModelIndex(13);
                    break;
                case 13:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(14);
                    break;
                case 14:
                    setModelIndex(14);
                    break;
                case 15:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(14);
                    break;
                case 16:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(15);
                    break;
                case 17:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(15);
                    break;
                case 18:
                    setModelIndex(15);
                    break;
                case 19:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(16);
                    break;
                case 20:
                    setModelIndex(16);
                    break;
                case 21:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(16);
                    break;
                case 22:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(17);
                    break;
                case 23:
                    setModelIndex(17);
                    break;
                case 24:
                    moveTo(clan::Point(getAnchor().x - 1, getAnchor().y));
                    setModelIndex(17);
                    break;
                default:
                    animationCounter = 0;
                    break;
            }
            if(carrying)
            {
                setModelIndex(getModelIndex() + 18);
            }
            
        }
    }
    if(vSpeed == vMovementState::jump)
    { //In order not to hit the moon
        jumpTimeout--;
        if(jumpTimeout == 0)
        {
            freefall();
        }
    }
}

void lighthouse::Player::hStop()
{ //stop walking
    animationCounter = 0;
    if(hSpeed == hMovementState::left)
    {
        hSpeed = hMovementState::standLeft;
        setModelIndex(0);
    }
    if(hSpeed == hMovementState::right)
    {
        hSpeed = hMovementState::standRight;
        setModelIndex(1);
    }
    if(carrying)
    {
        setModelIndex(getModelIndex() + 18);
    }
}

void lighthouse::Player::moveLeft()
{
    hSpeed = hMovementState::left;
}

void lighthouse::Player::moveRight()
{
    hSpeed = hMovementState::right;
}

void lighthouse::Player::freefall()
{
    vSpeed = vMovementState::fall;
    jumpTimeout = 0;
}
void lighthouse::Player::jump()
{
    vSpeed = vMovementState::jump;
    jumpTimeout = gndJumpTimeout;
}

void lighthouse::Player::vStop()
{
    vSpeed = vMovementState::stand;
    jumpTimeout = 0;
}

void lighthouse::Player::startInteract(lighthouse::Interactive* nearestObject)
{
    if(hand == nullptr && nearestObject != nullptr)
    {
        hand = nearestObject;
        if(nearestObject->getFlags()[0])
        {
            carrying = true;
        }
        nearestObject->beginInteraction(*this);
    }
    else
    {
        //if this is a carryable, it should drop itself and set carrying to false and release the hand.
        hand->beginInteraction(*this);
        if(hand->getFlags()[0])
        {
            carrying = false;
            hand = nullptr;
        }
    }
}

void lighthouse::Player::stopInteract()
{
    hand->endInteraction(*this);
    if(!carrying) hand = nullptr;
}
