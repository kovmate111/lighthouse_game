/*
 * <Wooden box object implementation>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../headers/globals_includes.h"
#include "../headers/WoodenBox.h"

lighthouse::WoodenBox::WoodenBox (
    const clan::Point& initialAnchor
) :
    ICarryable(
        std::string("textures/"),
        1,
        subpaths,
        initialAnchor,
        true,
        std::vector<bool>({false, false, false, false})
    )
{}

void lighthouse::WoodenBox::beginInteraction(lighthouse::Player& player)
{
    unsigned newX, newY;
    if(inHand)
    {
        //Drop it near the player
        if((player.getHorizontal() == hMovementState::standLeft || player.getHorizontal() == hMovementState::left) && !(player.getAnchor().x < getSize().width))
        { //drops it out to the left
            newX = player.getAnchor().x - getSize().width;
            newY = player.getAnchor().y + player.getSize().height - getSize().height;
        }
        if((player.getHorizontal() == hMovementState::standRight || player.getHorizontal() == hMovementState::right) && !(player.getAnchor().x + player.getSize().width + getSize().width > lighthouse::LighthouseGlobal::getLevelSize().width))
        { //drops it out to the right
            newX = player.getAnchor().x + player.getSize().width;
            newY = player.getAnchor().y + player.getSize().height - getSize().height;
        }
        inHand = false;
    }
    else
    {
        //Move to players hand anchor
        if(player.getHandAnchor() == clan::Point(-1, -1))
        {
            throw(std::logic_error("lighthouse::Player state corrupted"));
        }
        newX = player.getHandAnchor().x + player.getAnchor().x;
        newY = player.getHandAnchor().y + player.getAnchor().y;
        inHand = true;
    }
    moveTo(clan::Point(newX, newY));
    
}

void lighthouse::WoodenBox::endInteraction(lighthouse::Player& player)
{
}

void lighthouse::WoodenBox::draw()
{
    getSprite().draw(lighthouse::LighthouseGlobal::getCanvas(), (getAnchor().x - lighthouse::LighthouseGlobal::getScreenAnchor().x)*lighthouse::LighthouseGlobal::getUpscaleFactor(), (getAnchor().y - lighthouse::LighthouseGlobal::getScreenAnchor().y)*lighthouse::LighthouseGlobal::getUpscaleFactor());
}

void lighthouse::WoodenBox::update()
{
    //Do falling
    if(falling)
    {
        if(getAnchor().y < lighthouse::LighthouseGlobal::getLevelSize().width)
        {
            moveTo(clan::Point(getAnchor().x, getAnchor().y + 1));
        } else falling = false;
    }
    //Collision detection done by level update
    //Moving when carried done by player
    //Nothing to do
    
}
