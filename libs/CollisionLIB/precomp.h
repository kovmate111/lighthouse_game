#pragma once

#include <ClanLib-4.0/ClanLib/core.h>
#include <ClanLib-4.0/ClanLib/application.h>
#include <ClanLib-4.0/ClanLib/display.h>
#ifdef WIN32
#include <ClanLib-4.0/ClanLib/d3d.h>
#endif
#include <ClanLib-4.0/ClanLib/gl.h>
#include <cmath>
#include <ClanLib-4.0/ClanLib/xml.h>

