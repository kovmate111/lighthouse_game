add_library(testLevel STATIC testLevel.cpp)
target_include_directories(testLevel SYSTEM PUBLIC /usr/include/ClanLib-4.0/)
target_include_directories(testLevel PUBLIC ../../../libs/CollisionAPI/ PUBLIC ../../../headers/)
add_dependencies(testLevel collision_lib lighthouse_headers lighthouse_sources)
