/*
 * <Test level for the game.>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "../../../headers/globals_includes.h"
#include "../../../headers/Level.h"
#include "../../../headers/WoodenBox.h"

namespace lighthouse
{
    class TestLevel : public Level
    {
    private:
        
        static clan::FileSystem levelFs;
        
        /* ____DESCRIPTION__________________________
         * Level size is 320x160.
         * Player starts at (0, 90).
         * Terrain is a single line at 122.
         * There is a wooden box at (60, 106).
         * The goal is to take that box to (304, 106).
         */
        lighthouse::WoodenBox box1;
        static const clan::Size levelSize;
        static const clan::Point playerStart;
        static const clan::Point box1Start;
        static const clan::Point box1Dest;
        
    public:
        TestLevel();
        
        bool update(lighthouse::Player & player) override;
        
        lighthouse::Interactive* getNearestInteractive(const lighthouse::Player& player) override;
        
        void rescale() override;
    };
}
