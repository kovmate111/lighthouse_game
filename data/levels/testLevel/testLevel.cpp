/*
 * <Test level details and updating>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "../../../headers/globals_includes.h"
#include "testLevel.h"

/* ____DESCRIPTION__________________________
 * Level size is 320x160.
 * Player starts at (0, 90).
 * Terrain is a single line at 122.
 * There is a wooden box at (60, 106).
 * The goal is to take that box to (304, 106).
 */
const clan::Size lighthouse::TestLevel::levelSize = clan::Size(320, 160);
const clan::Point lighthouse::TestLevel::playerStart = clan::Point(0, 90);
const clan::Point lighthouse::TestLevel::box1Start = clan::Point(60, 106);
const clan::Point lighthouse::TestLevel::box1Dest = clan::Point(304, 106);

clan::FileSystem lighthouse::TestLevel::levelFs = clan::FileSystem("./");

lighthouse::TestLevel::TestLevel() :
Level(
    levelSize,
    levelFs,
    playerStart
),
box1(
    box1Start
)
{
    
}

bool lighthouse::TestLevel::update(lighthouse::Player& player)
{
    //First draw the background
    levelBackground.draw(lighthouse::LighthouseGlobal::getCanvas(), -(lighthouse::LighthouseGlobal::getScreenAnchor().x*lighthouse::LighthouseGlobal::getUpscaleFactor()), -(lighthouse::LighthouseGlobal::getScreenAnchor().y*lighthouse::LighthouseGlobal::getUpscaleFactor()));
    
    //Update the box
    if(box1.isInHand())
    {
        // It is in the hand. Move it with the player. Do not fall.
        box1.setFalling(false);
        box1.moveTo(player.getHandAnchor());
    }
    else
    {
        //It is not in the hand. Do collision detection on the platform.
        if(box1.getCollider().collide(terrainCollider))
        {
            box1.setFalling(false);
        }
        else
        {
            box1.setFalling(true);
        }
        box1.update();
    }
    //Draw the box
    box1.draw();
    
    //Player collision on the platform
    if(player.getCollider().collide(terrainCollider))
    {
        if(player.getVertical() == vMovementState::fall)
        player.vStop();
        if(player.getVertical() == vMovementState::jump)
        {
            player.freefall();
        }
    }
    else
    {
        if(player.getVertical() == vMovementState::stand)
        {
            player.freefall();
        }
    }
    //Player collision on the box
    if(!box1.isInHand())
    {
        if(player.getCollider().collide(box1.getCollider()))
        {
            //landing on it
            if(player.getAnchor().y <= box1.getAnchor().y - player.getSize().height && player.getVertical() == vMovementState::fall)
            {
                player.vStop();
            }
            //bumping into it from the side
            if((player.getHorizontal() == hMovementState::left || player.getHorizontal() == hMovementState::right) && player.getAnchor().y > box1.getAnchor().y - player.getSize().height)
            {
                player.hStop();
            }
        }
    }
    //update player
    player.update();
    
    //draw the player
    player.draw();
    
    //check for level completion
    return(box1.getAnchor() == box1Dest);
}

lighthouse::Interactive * lighthouse::TestLevel::getNearestInteractive(const lighthouse::Player& player)
{
    if(!box1.isInHand())
    {
        if(abs(player.getAnchor().x - box1.getAnchor().x) < player.getSize().width) return &box1;
        else return nullptr;
    }
    else return nullptr;
}

void lighthouse::TestLevel::rescale()
{
    levelBackground.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
    terrainCollider.set_scale(lighthouse::LighthouseGlobal::getUpscaleFactor(), lighthouse::LighthouseGlobal::getUpscaleFactor());
    box1.rescale();
}

