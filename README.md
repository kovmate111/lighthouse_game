# Lighthouse

A 2D puzzle game with movable light-emitting / light-receiving blocks, steam pipes and valves and more, in pixel-art Steampunk style, written in C++ using the ClanLib library for graphics and keyboard input.

The game mechanics are mostly inspired by [Supertux 2](https://www.supertux.org/index.html).

All graphics are made by Végh Márton using [Aseprite](https://www.aseprite.org/).

Originally it was a homework assignment for the Basics of Programming 2 course of Forstner Bertalan Dr. at [Budapest University of Technology and Economics](bme.hu), but the project had overgrown that and turned into a hobby project.

Feel free to report issues and send merge requests with useful additions/optimizations, but don't expect insanely active development as we are students with a lot of things to do, and this is only a side project for us at the moment.

## Installation

Currently there isn't much to install, but you can try to build the game with a demo level from source.
First, you need to build and install [ClanLib](github.com/sphair/ClanLib). Follow the instructions there.
Then, you have to download the source code with the Clone button above (or git clone).
In the folder where you have downloaded Lighthouse, issue a `cmake` command in Linux, or use Visual Studio in Windows to buid it. Alternatively, you can use [KDevelop](kdevelop.org) to buid the game (if you have it; check it out, it is a great IDE).
When 0.1 will be released, there will be a proper binary installer. With 1.0, there will be a Windows installer, and Linux binary (whether Flatpak, AppImage or native package, is yet to be decided).