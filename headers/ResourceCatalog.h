/*
 * <Sprite/collider collection object for handling image assets>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"

/**
 * @brief A namespace holding all classes of the game
 */
namespace lighthouse
{
    /**
     * @brief An image, a corresponding collision outline and a common filename without extension.
     */
    struct model
    {
        clan::Sprite sprite;
        std::string subpath;
        CollisionOutline outline;
    };
    
    /**
     * @brief Resource manager class. Loads and stores sprites and collision outlines, making them available for other classes.
     */
    class ResourceCatalog
    {
    private:
        // -------- FIELDS -------- //
        
        /**
         * @brief The number of visual states stored in here
         */
        const unsigned resourcesNum;
        
        /**
         * @brief The subdirectory path of the containing object inside the global sprite and collider filesystems.
         */
        const std::string commonSubdir;
        
        /**
         * @brief The level of accuracy the colliders are generated with.
         */
        const OutlineAccuracy colliderAccuracy;
        
        /**
         * @brief An array to hold all the visual states
         */
        model* array;
    
    public:
        // -------- CONSTRUCTOR -------- //
        
        /**
         * @brief Constructor
         * Handles resource loading and collider generation.
         * PLEASE SPECIFY PATHS IN "<dirname>/" FORMAT!!
         * Filesystems and Canvas are determined from the Global.
         * For the Scale property of the sprites, the upscaleFactor obtained from Global is used initially.
         * 
         * @param pResourcesNum (const unsigned int&) The number of visual states to handle.
         * @param pCommonSubdir (const std::string&) The subdirectory path name of the containing object inside global sprite and collider filesystems.
         * @param pSubpathArray (const std::string*) An array containing all filenames corresponding to visual states, inside pCommonSubdir, without extension.
         * @param pColliderAccuracy (const OutlineAccuracy&) The accuracy with which to generate the collision outlines.
         */ 
        ResourceCatalog(
            const unsigned int& pResourcesNum,
            const std::string& pCommonSubdir,
            const std::string* pSubpathArray,
            const OutlineAccuracy& pColliderAccuracy
        );
        
        // -------- DESTRUCTOR -------- //
        
        /**
         * @brief Destructor
         * Unloads resources and frees memory.
         */
        ~ResourceCatalog();
        
        // -------- GETTERS -------- //
        
        model& getModelByIndex(const unsigned int& index);
        
        inline const OutlineAccuracy& getAccuracy() const { return colliderAccuracy; }
        
        inline const unsigned& getResNum() const { return resourcesNum; }
        
        // -------- OTHER FUNCTIONS -------- //
        
        /**
         * @brief Sets the Scale property of all sprites and colliders to the (presumably changed) upscaleFactor from Global.
         */
        void rescale();
    };
}
