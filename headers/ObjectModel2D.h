/*
 * <Basic 2D object model class with virtual drawing method>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"
#include "ResourceCatalog.h"

/**
 * @brief A namespace holding all classes of the game
 */
namespace lighthouse
{
    /**
     * @brief Abstract ancestor class for all members and functions common among 2D objects drawn on the screen
     */
    class ObjectModel2D
    {
    private:
        
        // -------- MEMBERS -------- //
        
        /**
         * @brief Positon of rectangular object relative to the level.
         * The screen position is calculated by adding the screen anchor to it.
         * Position anchors always represent top-left corner.
         * The unit they are given in is visible pixels.
         * This gets multiplied by the upscanling factor calculated from the actual screen resolution.
         */
        clan::Point position;
        
        /**
         * @brief Size of rectangular object.
         * The unit they are given in is visible pixels.
         * This gets multiplied by the upscanling factor calculated from the actual screen resolution.
         * Having objects shrink and expand is not relevant.
         */
        const clan::Size size;
        
        /**
         * @brief Solidity flag.
         * True means the object is solid and collision detection is necessary.
         * False means it is not solid and will be drawn as background, and no collision detection is necessary.
         */
        bool solid;
        
        /**
         * @brief Index of the current graphical and physical representation of the object.
         */
        unsigned int currentModelIndex;
        
        /**
         * @brief Bundled resource catalog for the object. Handles Sprite and Collider loading.
         * Indexable through getSpriteByIndex and getOutlineByIndex functions.
         * The currentModel pointer must be set by update() and other functs.
         */
        ResourceCatalog resCatalog;
        
    protected:
        
        // -------- CONSTRUCTOR -------- //
        
        /**
         * @brief Constructor with initial values
         * Tasks:
         * - Member initialization
         * - Texture loading from files (populating ResourceCatalog)
         * - Collision outline generation
         * 
         * @param pCommonSubdir (const std::string&) The subdirectory path name where the resources of the object are found inside the respective filesystems.
         * @param pResourcesNum (const unsigned int&) The number of visual states (and therefore collision outlines) the object can have.
         * @param pSubpathArray (const std::string*) An array of strings containing the path and filename of visual states inside pCommonSubdir.
         * @param initialAnchor (const clan::Point&) The starting anchor of the object (relative to level origin, in virtual pixels).
         * @param pSize (const clan::Size&) The size of the object (in virtual pixels).
         * @param initialSolidity (bool) The initial solidity of the object. Can be changed later (PiezoBlock for example).
         * @param pAccuracy (const OutlineAccuracy&) Collider outline accuracy
         */
        ObjectModel2D(
            const std::string& pCommonSubdir,
            const unsigned int& pResourcesNum,
            const std::string* pSubpathArray,
            const clan::Point& initialAnchor,
            const clan::Size& pSize,
            bool initialSolidity,
            const OutlineAccuracy& pAccuracy
        );
        
    public:
        
        // -------- VIRTUAL FUNCTIONS -------- //
        
        /**
         * @brief Function to draw the object on the screen.
         * 
         * The canvas on which the drawing occours is determined from the Global.
         * This represents the actual screen. Its topleft corner is screenAnchor compared to the level origin, in virtual pixels.
         * The screen anchor is determined from the Global.
         * It is the relative origin of the drawing area compared to the level origin, in virtual pixels.
         */
        virtual void draw() = 0;
        
        /**
         * @brief update internal state according to surroundings
         * 
         * The screen anchor is determined from the Global.
         * It is the relative origin of the drawing area compared to the level origin, in virtual pixels.
         * The LevelSize (size of current level in virtual pixels) is determined from the Global.
         */
        virtual void update() = 0;
        
        // -------- GETTERS -------- //
        
        inline const clan::Point getAnchor() const { return position; } //relative to level, in virtual pixels
        
        inline const clan::Size getSize() const { return size; } //in virtual pixels
        
        inline const bool isSolid() const { return solid; }
        
        inline clan::Sprite& getSprite() { return resCatalog.getModelByIndex(currentModelIndex).sprite; }
        
        inline CollisionOutline getCollider() { return resCatalog.getModelByIndex(currentModelIndex).outline; }
        
        inline const unsigned int& getModelIndex() const { return currentModelIndex; }
        
        inline const OutlineAccuracy& getColliderAccuracy() const { return resCatalog.getAccuracy(); }
        
        // -------- SETTERS -------- //
        
        /**
         * @brief Moves object to the desired location (given relative to the level origin, in virtual pixels)
         * after checking whether it will still fit inside the level.
         * 
         * Mandatory parameters:
         * @param newPos (const clan::Point&) The desired new location of the object anchor, relative to the level origin, in virtual pixels
         * 
         * LevelSize is determined from the Global.It is used for sanity checking. Nothing should ever leave a level.
         */
        void moveTo(const clan::Point& newPos);
        
        /**
         * @brief Makes the object solid
         */
        inline void solidify() { solid = true; }
        /**
         * @brief Makes the object not solid
         */
        inline void unsolidify() { solid = false; }
        
        /**
         * @brief Sets current model index (does checking)
         * 
         * @param newIdx (const unsigned&) The model index to set as current
         * @throws std::out_of_range if the index is bad
         */
        void setModelIndex(const unsigned& newIdx);
        
        inline void rescale() { resCatalog.rescale(); }
        
    };
    
}
