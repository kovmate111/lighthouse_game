/*
 * <Wooden box object class.>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"
#include "ObjectInterfaces.h"
#include "Player.h"

namespace lighthouse
{
    class WoodenBox : public ICarryable
    {
    private:
        
        // FIELDS
        
        /**
         * @brief Resource subpaths related to box.
         */
        const std::string subpaths[1] = 
        {
            std::string("woodenbox")
        };
        
    public:
        
        // -------- CONSTRUCTOR -------- //
        
        /**
         * @brief Constructor
         * LevelSize and Canvas are determined from the Global.
         * 
         * @param initialAnchor (const clan::Point&)
         */
        WoodenBox(
            const clan::Point& initialAnchor
        );
        
        // -------- OTHER FUNCTIONS -------- //
        
        /**
         * @brief drawing implementation
         * 
         * Canvas and ScreenAnchor are determined from the Global.
         */
        void draw() override;
        
        /**
         * @brief update implementation
         * LevelSize and Canvas are determined from the Global.
         */
        void update() override;
        
        void beginInteraction(lighthouse::Player& player) override;
        void endInteraction(lighthouse::Player& player) override;
    };
}
