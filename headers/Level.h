/*
 * <Level loading and handling class>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"
#include "ObjectInterfaces.h"
#include "Player.h"
// Actual levels should include the elements they actually use

namespace lighthouse
{
    class Level
    {
        //TODO document this better!!!
    private:
        
    protected:
        
        // -------- FIELDS -------- //
        // A level has a size, a background, a foreground collider (made from a png mask)
        // and a collection of interactive objects with their initial states and finished states.
        // This base class doesn't have the objects. It has a virtual update() method that takes care of everything.
        // Actual levels will be derived from this.
        // If update() returns true, the level is complete. update() ensures that all objects update properly. Main update only has to call this. A reference to Player is passed around.
        // Each level should have a subdir under data/levels with the background, terrain mask and results.txt. levelDataFs points to that subdir. This allows zipping up levels.
        // Main app has a polymorph pointer to the curretn level and uses getPlayerAnchor() to retrieve the player's initial coords.
        // Level is init before Player.
        // update() should draw the level background and check for ground collison.
        //
        const clan::Size levelSize;
        
        clan::Sprite levelBackground;
        
        CollisionOutline terrainCollider;
        
        clan::FileSystem levelDataFs;
        
        //Player initial data
        const clan::Point initialPlayerAnchor;
        //Player will face right
        
        Level(
            const clan::Size& pLevelSize,
            clan::FileSystem& pLevelDataFs,
            const clan::Point& pPlayerInitialAnchor
        );
        
    public:
        
        inline const clan::Size& getLevelSize() const { return levelSize; }
        
        inline const clan::Point& getPlayerAnchor() const { return initialPlayerAnchor; }
        
        virtual bool update(lighthouse::Player& player) = 0;
        
        virtual lighthouse::Interactive* getNearestInteractive(const lighthouse::Player& player) = 0;
        
        virtual void rescale() = 0;
    };
    
}
