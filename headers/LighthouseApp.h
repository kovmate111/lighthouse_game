/*
 * <Main app class for the Lighthouse game.>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"
#include "ObjectInterfaces.h"
#include "Player.h"
#include "../data/levels/testLevel/testLevel.h"

namespace lighthouse
{
    /**
    * @todo write docs
    */
    class LighthouseApp : public clan::Application
    {
    private:
        // -------- FIELDS -------- //
        
        /**
        * @brief Stores whether the app has to quit on the next cycle or not
        */
        bool quit=false;
        
        /**
        * @brief The main window in which the game is displayed. It is a fullscreen window on the primary screen.
        */
        clan::DisplayWindow mainWindow;
        
        /**
        * @brief A slot container to handle various events, like window actions and keyboard input.
        */
        clan::SlotContainer slotContainer;
        
        /**
        * @brief A canvas to draw all the stuff on
        */
        clan::Canvas mainCanvas;
        
        /**
         * @brief Image filesystem
         */
        clan::FileSystem sprites;
        
        /**
         * @brief Collision filesystem
         */
        clan::FileSystem colliders;
        
        /**
        * @brief Game timer
        */
        clan::GameTime gameTime;
        
        /**
        * @brief Button mappings
        */
        clan::InputCode walkRightKey;
        clan::InputCode walkLeftKey;
        clan::InputCode jumpKey;
        clan::InputCode interactKey;
        
        /**
        * @brief An array storing all the levels of the game.
        * Levels are dynamically loaded when the player starts them,
        * but the Level objects are kept in memory the whole time.
        */
        static const unsigned numberOfLevels = 1; //Demo has only one level, more will be added later
        //lighthouse::Level *levels;
        
        /**
        * @brief A pointer to the level currently being played.
        */
        //lighthouse::Level *currentLevel;
        //clan::Sprite TryLevel1;
        lighthouse::TestLevel testLevel;
        
        /**
        * @brief The character controlled by the player.
        * Keyboard events will get passed through.
        */
        lighthouse::Player player;
        
        // -------- FUNCTIONS -------- //
        
        /**
        * @brief Keypress event handler (passes the event through to the appropriate object)
        * 
        * @param key (const clan::InputEvent&) the keypress event to handle.
        */
        void on_input_down(const clan::InputEvent &key);
        
        /**
        * @brief Key release event handler (passes the event through to the appropriate object)
        * 
        * @param key (const clan::InputEvent&) the key release event to handle.
        */
        void on_input_up(const clan::InputEvent &key);
        
        static clan::DisplayWindowDescription makeLighthouseWindow();
        
    public:
        // -------- FUNCTIONS -------- //
        
        /**
        * @brief Default constructor.
        * Handles all initialization of the application.
        */
        LighthouseApp();
        
        /**
        * @brief Updates the game states and draws the content on the screen.
        * To be called for every frame.
        * Optimal framerate is 24FPS, but animations consist of 4 or 8 frames.
        * 
        * @return true if the game has to continue, false if the game has to quit.
        */ 
        bool update() override;
        
    };
}
