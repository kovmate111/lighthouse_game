/*
 * <A header file containing all object behaviour related property interfaces>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"
#include "ObjectModel2D.h"

/**
 * @brief A namespace holding all classes of the game
 */
namespace lighthouse
{
    //dummy class
    class Player;
    /**
     * @brief Basic interface allowing interactivity. Enables interact() methods.
     */
    class Interactive : public ObjectModel2D
    {
    protected:
        
        // -------- FIELDS -------- //
        
        /**
         * @brief interface flags to signal back what you can do with it that needs care
         */
        const bool carryable;
        const bool lightsource;
        const bool lightreactive;
        const bool steamsource;
        const bool steamreactive;
        
        // CONSTRUCTOR
        
        /**
         * @brief Default constructor. Only for derived classes.
         * 
         * @param pCommonSubdir (const std::string&) The common subdirectory path name of the object in the global collider and sprite filesystems.
         * @param pResourcesNum (const unsigned int&) The number of visual states the object can have.
         * @param pSubpathArray (const std::string*) An array containing all file names corresponding to visual states, inside pCommonSubdir, without extension.
         * @param initialAnchor (const clan::Point&) The starting anchor of the object (relative to level origin, in virtual pixels).
         * @param initialSolidity (bool) Signals whether it should be solid by default
         * @param pFlags (const std::vector<bool>) For setting the interface flags
         */
        Interactive (
            const std::string& pCommonSubdir,
            const unsigned int& pResourcesNum,
            const std::string* pSubpathArray,
            const clan::Point& initialAnchor,
            bool initialSolidity,
            const std::vector<bool> pFlags
        );
        
    public:
        
        // -------- GETTERS -------- //
        
        /**
         * @brief Returns the interaction type flags in a nice vector.
         */
        inline const std::vector<bool> getFlags() const { return std::vector<bool>({carryable, lightsource, lightreactive, steamsource, steamreactive}); }
        
        // -------- OTHER FUNCTIONS -------- //
        
        /**
         * @brief Called when the player starts to interact with it.
         * LevelSize is determined from the Global.
         * 
         * @param player (lighthouse::Player&) The player that interacts with the object.
         */
        virtual void beginInteraction(lighthouse::Player& player) = 0;
        
        /**
         * @brief Called when the player stops to interact with it.
         * LevelSize is determined from the Global.
         * 
         * @param player (lighthouse::Player&) The player that interacts with the object.
         */
        virtual void endInteraction(lighthouse::Player& player) = 0;
        
    };
    
    /**
     * @brief ICarryable interface, for  things that the player can misplace.
     */
    class ICarryable : public Interactive
    {
    protected:
        /**
         * @brief Flag to indicate whether or not it should follow the player
         */
        bool inHand;
        /**
         * @brief Flag to indicate whether it is falling
         */
        bool falling;
        
        // -------- CONSTRUCTOR -------- //
        
        /**
         * @brief Default constructor. Only for derived classes.
         * 
         * @param pCommonSubdir (const std::string&) The common subdirectory path name of the object in the global collider and sprite filesystems.
         * @param pResourcesNum (const unsigned int&) The number of visual states the object can have.
         * @param pSubpathArray (const std::string*) An array containing all file names corresponding to visual states, inside pCommonSubdir, without extension.
         * @param initialAnchor (const clan::Point&) The starting anchor of the object (relative to level origin, in virtual pixels).
         * @param initialSolidity (bool) Signals whether it should be solid by default
         * @param pFlags (const std::vector<bool>) For setting the interface flags
         */
        ICarryable (
            const std::string& pCommonSubdir,
            const unsigned int& pResourcesNum,
            const std::string* pSubpathArray,
            const clan::Point& initialAnchor,
            bool initialSolidity,
            const std::vector<bool> pFlags
        );
    public:
        
        // -------- GETTERS -------- //
        
        inline const bool isInHand() const { return inHand; }
        
        inline const bool isFalling() const { return falling; }
        
        // -------- SETTERS -------- //
        
        inline void setInHand(const bool h) { inHand = h; }
        
        inline void setFalling(const bool f) { falling = f; }
        
        // -------- OTHER FUNCTIONS -------- //
    };
    
    //The demo ends here, but more interfaces would be coming, derived from ObjectModel2D. The actual game objects are inheriting from these interfaces, allowing for great modularity and versatility.
}
