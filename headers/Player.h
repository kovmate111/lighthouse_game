/*
 * <Player class of the game. Stores all player-related game states and
 * functions related to displaying the player character on screen.>
 * Copyright (C) 2020 Kovács Máté <koma111@hotmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "globals_includes.h"
#include "ObjectModel2D.h"
#include "ObjectInterfaces.h"

/**
 * @brief A namespace holding all classes of the game
 */
namespace lighthouse
{
    /**
     * @brief Horizontal movement state
     * Used for walking, rope climbing and gliding.
     * 
     * stand:
     *  the character is not moving horizontally.
     * left:
     *  the character is moving to the left at constant speed.
     *  When the walkLeft key is pressed, this happens.
     *  When it is released or the player collides with something to the left,
     *  the next state is stand.
     * right:
     *  the character is moving to the right at constant speed.
     *  When the walkRight key is pressed, this happens.
     *  When it is released or the player collides with something to the right,
     *  the next state is stand.
     */
    enum class hMovementState { standLeft, standRight, left, right };
    
    /**
     * @brief Vertical movement state
     * Used at all times.
     * 
     * stand:
     *  the character is climbing a rope,
     *  or standing/walking on a platform or solid block,
     *  so not moving vertically.
     * jump:    
     *  the character is moving upwards at a gradually decreasing speed.
     *  When he starts to jump from a platform or solid block,
     *  a PlatformJumpTiemout countdown is started
     *  (LadderJumpTimeout for jumping off of ladders added later),
     *  at the end of which the character is either on something solid or starts to fall.
     *  If the jump is cancelled (the jump key is released)
     *  or he collides with something above, he also starts to fall.
     *  If he manages to catch a rope before starting to fall, the next state is stand.
     *  If he manages to catch a ladder, the next state is climbLadder.
     * fall:
     *  the character is moving downwards at a gradually increasing speed.
     *  When he catches a rope or ladder,
     *  or collides with something solid below, he stops falling.
     *  In the ladder case, the new state is climbLadder, otherwise stand.
     * climbLadder:
     *  the character is moving upwards at constant speed.
     *  When the Interaction key is released or the ladder ends
     *  or he collides with something above, the next state is fall,
     *  and his hands are emptied.
     *  When Jump is pressed while climbing a ladder,
     * a countdown of LadderJumpTimeout is started and the next state is jump.
     */
    enum class vMovementState { stand, jump, fall, climbLadder };
    
    /**
     *@brief Player class for storing the main character and his data
     */    
    class Player : public lighthouse::ObjectModel2D
    {
    private:
        
        // -------- FIELDS -------- //
        
        /**
         * @brief The object the Player is holding.
         * 
         * A pointer to the Interactive object the player is holding.
         * nullpttr when the player isn't holding an interactive block.
         * Can point to ladders and ropes as well, which should be single objects
         * and not a composition of tiles.
         */
        lighthouse::Interactive *hand;
        
        /**
         * @brief Carry flag
         */
        bool carrying;
        
        /**
         * @brief Horizontal movement state
         * 
         * Horizontal movement of the player. Can be left, right and stand.
         * Used for gliding, walking and rope climbing too.
         * The drawing function will calculate the actual px/frame movement speed
         * of objects, this is just for figuring out the direction when selecting
         * which pictures to use and what direction to move the player in.
         */
        lighthouse::hMovementState hSpeed;
        
        /**
         * @brief Vertical movement state
         * 
         * Vertical movement of the player. Can be jump, fall, climbLadder or stand.
         * The drawing function will calculate the actual px/frame movement speed
         * of objects, this is just for figuring out the direction when selecting
         * which pictures to use and what direction to move the player in,
         * and what actions are possible.
         */
        lighthouse::vMovementState vSpeed;
        
        /**
         * @brief Resource subpaths related to player. Filenames hardcoded.
         * 36 in total,
         * 18 with carried block.
         */
        const std::string subpaths[36] =
        {
        /* 00 */    std::string("walk/stand-left"),
        /* 01 */    std::string("walk/stand-right"),
        /* 02 */    std::string("walk/walk-left-1"),
        /* 03 */    std::string("walk/walk-left-2"),
        /* 04 */    std::string("walk/walk-left-3"),
        /* 05 */    std::string("walk/walk-left-4"),
        /* 06 */    std::string("walk/walk-left-5"),
        /* 07 */    std::string("walk/walk-left-6"),
        /* 08 */    std::string("walk/walk-left-7"),
        /* 09 */    std::string("walk/walk-left-8"),
        /* 10 */    std::string("walk/walk-right-1"),
        /* 11 */    std::string("walk/walk-right-2"),
        /* 12 */    std::string("walk/walk-right-3"),
        /* 13 */    std::string("walk/walk-right-4"),
        /* 14 */    std::string("walk/walk-right-5"),
        /* 15 */    std::string("walk/walk-right-6"),
        /* 16 */    std::string("walk/walk-right-7"),
        /* 17 */    std::string("walk/walk-right-8"),
        /* 18 */    std::string("walk_woodenbox/stand-left-woodenbox"),
        /* 19 */    std::string("walk_woodenbox/stand-right-woodenbox"),
        /* 20 */    std::string("walk_woodenbox/walk-left-woodenbox-1"),
        /* 21 */    std::string("walk_woodenbox/walk-left-woodenbox-2"),
        /* 22 */    std::string("walk_woodenbox/walk-left-woodenbox-3"),
        /* 23 */    std::string("walk_woodenbox/walk-left-woodenbox-4"),
        /* 24 */    std::string("walk_woodenbox/walk-left-woodenbox-5"),
        /* 25 */    std::string("walk_woodenbox/walk-left-woodenbox-6"),
        /* 26 */    std::string("walk_woodenbox/walk-left-woodenbox-7"),
        /* 27 */    std::string("walk_woodenbox/walk-left-woodenbox-8"),
        /* 28 */    std::string("walk_woodenbox/walk-right-woodenbox-1"),
        /* 29 */    std::string("walk_woodenbox/walk-right-woodenbox-2"),
        /* 30 */    std::string("walk_woodenbox/walk-right-woodenbox-3"),
        /* 31 */    std::string("walk_woodenbox/walk-right-woodenbox-4"),
        /* 32 */    std::string("walk_woodenbox/walk-right-woodenbox-5"),
        /* 33 */    std::string("walk_woodenbox/walk-right-woodenbox-6"),
        /* 34 */    std::string("walk_woodenbox/walk-right-woodenbox-7"),
        /* 35 */    std::string("walk_woodenbox/walk-right-woodenbox-8")
        };
        //TODO More coming later (ropes, ladders, flashlight, and other good stuff)
        
        /**
         * @brief Hand anchor points relative to Player anchor for different states
         */
        const clan::Point handAnchors[18] =
        {
            clan::Point(0, 6),  //stand-left-woodenbox
            clan::Point(16, 6), //stand-right-woodenbox
            clan::Point(0, 6),  //walk-left-woodenbox-1
            clan::Point(0, 6),  //walk-left-woodenbox-2
            clan::Point(0, 6),  //walk-left-woodenbox-3
            clan::Point(0, 6),  //walk-left-woodenbox-4
            clan::Point(0, 6),  //walk-left-woodenbox-5
            clan::Point(0, 6),  //walk-left-woodenbox-6
            clan::Point(0, 6),  //walk-left-woodenbox-7
            clan::Point(0, 6),  //walk-left-woodenbox-8
            clan::Point(16, 6), //walk-right-woodenbox-1
            clan::Point(16, 6), //walk-right-woodenbox-2
            clan::Point(16, 6), //walk-right-woodenbox-3
            clan::Point(16, 6), //walk-right-woodenbox-4
            clan::Point(16, 6), //walk-right-woodenbox-5
            clan::Point(16, 6), //walk-right-woodenbox-6
            clan::Point(16, 6), //walk-right-woodenbox-7
            clan::Point(16, 6)  //walk-right-woodenbox-8
        };
        
        /**
         * @brief Animation and jump timeout counters, update() and movement functions will adjust
         * 
         * Jumping and falling is constant at first, weird I know
         * TODO real accelerated/decelerated jumping based on gravity
         */
        unsigned int animationCounter;
        unsigned int jumpTimeout;
        static const unsigned int gndJumpTimeout = 24; //Moving up for 24 frames so he starts to fall after 1s
        //Ladder timeout might get added later
        
    public:
        
        // -------- GETTERS -------- //
        
        inline const lighthouse::Interactive& getHand() const { return *hand; }
        
        inline const lighthouse::hMovementState getHorizontal () const { return hSpeed; }
        
        inline const lighthouse::vMovementState getVertical () const { return vSpeed; }
        
        inline const clan::Point getHandAnchor () const { return carrying ? (handAnchors[getModelIndex()-18]) : (clan::Point(-1, -1));  }
        
        inline const bool isCarrying() const { return carrying; }
        
        // -------- SETTERS -------- //
        
        void setHand(lighthouse::Interactive* newHand); //Is that actually needed?
        
        // -------- CONSTRUCTOR -------- //
        
        /**
         * @brief Default constructor
         * (position required, initial hand supported but not required)
         * LevelSize and main Canvas are determined from the Global.
         * 
         * Mandatory parameters:
         * @param initialAnchor (const clan::Point&) the initial (x,y) position of the character in visible pixels
         * 
         * Optional parameters:
         * @param initialHandState (lighthouse::Interactive*) the initial object in the character's hand (most common use-case is the Flashlight)
         */
        Player (
            const clan::Point& initialAnchor,
            lighthouse::Interactive* initialHandState = nullptr
        );
        
        // -------- OTHER FUNCTIONS -------- //
        
        /**
         * @brief Function to start moving left (only if not going right)
         * 
         * Collision stuff is not done here, that is handled by ClanLib and the main loop.
         * When the character bumps into something to the left, hStop() will be called.
         */
        void moveLeft ();
        
        /**
         * @brief Function to start moving right (only if not going left)
         * 
         * Collision stuff is not done here, that is handled by ClanLib and the main loop.
         * When the character bumps into something to the left, hStop() will be called.
         */
        void moveRight ();
        
        /**
         * @brief Function to stop horizontal movement
         * (when colliding horizontally or left/right key is released)
         */
        void hStop ();
        
        /**
         * @brief Function to start jumping (if standing on something solid or climbing a ladder)
         * 
         * There should be a counter in the main loop to stop going up after a while.
         * When the character collides upwards, the timeout is reached, or the jump key is released,
         * the character should start falling.
         * When a ladder is catched, he should start climbing.
         * When a rope is catched, he should stop vertical movement and stay right below the rope.
         * In the last 2 cases the Hand also has to be updated.
         * Catching a ladder or rope should not be possible with non-empty hands.
         */
        void jump ();
        
        /**
         * @brief Function to start falling (walking off a platform or ceasing to go up)
         * 
         * Downward collision detection is always needed.
         */
        void freefall ();
        
        /**
         * @brief Function to land on a solid object or grab a rope.
         * 
         * Vertical movement should cease.
         */
        void vStop ();
        
        /**
         * @brief Function to start climbing up a ladder.
         */
        void startClimbing ();
        
        /**
         * @brief Function to start interacting with an Interactive object.
         * 
         * If the hand is full, the object in the hand will be used.
         * If the hand is empty, the object given as parameter will be used.
         * LevelSize is determined from the Global.
         * Optional parameters:
         * @param nearestObject the object to interact with if the hand is empty.
         */
        void startInteract (lighthouse::Interactive* nearestObject);
        
        /**
         * @brief Function to stop interacting with an Interactive object.
         * 
         * The hand is used in all cases.
         * LevelSize is determined from the Global.
         */
        void stopInteract ();
        
        /**
         * @brief Function to draw the player on the screen.
         * Inherited from ObjectModel2D.
         * LevelSize, Canvas and ScreenAnchor are determined from the Global.
         */
        void draw() override;
        
        /**
         * @brief Function to update Player's internal states based on surroundings and collisions
         * Inherited from ObjectModel2D.
         * LevelSize is determined from the Global.
         */
        void update () override;
        
    };
    
}
